from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Salesperson, Customer, Sale, AutomobileVO


class Salesperson_Encoder(ModelEncoder):
    model = Salesperson
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]


class Customer_Encoder(ModelEncoder):
    model = Customer
    properties = [
        "first_name",
        "last_name",
        "address",
        "phone_number",
        "id",
    ]


class AutomobileVO_Encoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]


class Sale_Encoder(ModelEncoder):
    model = Sale
    properties = [
        "automobile",
        "salesperson",
        "customer",
        "price",
        "id",
    ]
    encoders = {
        "automobile": AutomobileVO_Encoder(),
        "salesperson": Salesperson_Encoder(),
        "customer": Customer_Encoder(),
    }


@require_http_methods(["GET", "POST"])
def api_get_all_salespeople(request, salesperson_id=None):
    if request.method == "GET":
        if salesperson_id is not None:
            salespeople = Salesperson.objects.filter(id=salesperson_id)
        else:
            salespeople = Salesperson.objects.all()

        return JsonResponse(
            {"salespeople": salespeople},
            encoder=Salesperson_Encoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=Salesperson_Encoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_salesperson(request, salesperson_id):
    try:
        salesperson = Salesperson.objects.get(id=salesperson_id)
        count, _ = salesperson.delete()
        return JsonResponse({"deleted": count > 0})
    except Salesperson.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid Salesperson ID"},
            status=404,
        )

@require_http_methods(["GET", "POST"])
def api_get_all_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()

        return JsonResponse(
            {"customers": customers},
            encoder=Customer_Encoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=Customer_Encoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_customer(request, customer_id):
    try:
        customer = Customer.objects.get(id=customer_id)
        count, _ = customer.delete()
        return JsonResponse({"deleted": count > 0})
    except Customer.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid Customer ID"},
            status=404,
        )

@require_http_methods(["GET"])
def api_get_all_AutoVOs(request):
    autoVO = AutomobileVO.objects.all()
    return JsonResponse(
        {"autoVO": autoVO},
        encoder=AutomobileVO_Encoder,
    )

@require_http_methods(["GET", "POST"])
def api_get_all_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales":sales},
            encoder=Sale_Encoder,
        )
    else:
        content = json.loads(request.body)
        try:
            print(content["price"])

            vin = content["automobile"]
            auto = AutomobileVO.objects.get(vin=vin)
            content["automobile"] = auto

            salespersonId = content["salesperson"]
            salesperson = Salesperson.objects.get(id=salespersonId)
            content["salesperson"] = salesperson

            customerId = content["customer"]
            customer = Customer.objects.get(id=customerId)
            content["customer"] = customer

        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid VIN"},
                status=400,
            )

        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Salesperson ID"},
                status=400,
            )

        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Customer ID"},
                status=400,
            )

        sale = Sale.objects.create(**content)
        return JsonResponse(
            sale,
            encoder=Sale_Encoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_sale(request, sale_id):
    try:
        sale = Sale.objects.get(id=sale_id)
        count, _ = sale.delete()
        return JsonResponse({"deleted": count > 0})
    except Sale.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid Sale ID"},
            status=404,
        )
