from django.urls import path
from .api_views import api_get_all_salespeople, api_delete_salesperson, api_get_all_customers, api_delete_customer, api_get_all_sales, api_delete_sale, api_get_all_AutoVOs

urlpatterns = [
    path("salespeople/", api_get_all_salespeople, name="api_get_all_salespeople"),
    path("salespeople/<int:salesperson_id>/", api_delete_salesperson, name="api_delete_salesperson"),
    path("customers/", api_get_all_customers, name="api_get_all_customers"),
    path("customers/<int:customer_id>/", api_delete_customer, name="api_delete_customer"),
    path("sales/", api_get_all_sales, name="api_get_all_sales"),
    path("sales/<int:sale_id>/", api_delete_sale, name="api_delete_sale"),
    path("autovos/", api_get_all_AutoVOs, name="api_get_all_AutoVOs")
]
