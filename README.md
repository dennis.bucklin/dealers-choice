# Dealer's Choice
Taking your dealership to the next level? Dealer's Choice is here to help!

Dealer's Choice tracks your Autos, Salespeople, Technicians, and Sales. Record your car sales and inventory, and track service appointments and vehicle histories all on our interactive single-page-application!

Team:

* Denny Bucklin - Service microservice
* Sam Madvig - Sales microservice

## Design

![Project diagram PNG](./project-diagram.png)

## Service microservice

AutomobileVO is a Value Object that copies 'vin' and 'sold' from the Automobile model in the Inventory microservice.

A poller updates the AutomobileVO with data from the Inventory service by a periodic Get request.

Service will also track Technicians and Appointments. The Appointment model has a technician property that is a Foreign Key to a Technician value. Deleting a technician removes all services attached to that entity.

Appointment also contains appointment date_time (a datetime value), reason (a string value), status (a string value), vin (a string value), and customer (a string value).

Technician also contains first_name, last_name, and employee_id (all string values).

## Sales microservice

The shoes microservice uses a poller to pull data ("vin" and "sold") from the Inventory service in order to populate a Value object - AutomobileVO. In addition there are three additional models that are used in the Sales microservice: Salesperson, Customer, and Sale.

Salesperson contains these variables - first_name, last_name, employee_id
Customer contains these variables - first_name, last_name, address, phone_number
Sale contains these variables - automobile (fk based on AutomobileVO), salesperson (fk based on Salesperson), customer (fk based on Customer), price (a decimal value)

The sales microservice will allow you to create and view customers, create and view salespeople, create and view sales, and also view sales based on salesperson.

Sales Value Object: The AutomobileVO

## How to run

This project is built on Docker. Using Docker-compose streamlines activation by packaging all the ports and configurations for each microservice in a single docker-compose.yml. It is recommended that container applications are run through docker to ensure an identical environment to the one used in development.

Install Docker from https://www.docker.com/ and download the program files from https://gitlab.com/sammadvig/project-beta then use the following steps to run this application:

1. From a command prompt, locate the root folder
2. Run "docker volume create beta-data"
3. Run "docker-compose build"
4. Run "docker-compose up"
5. Navigate to the main page: http://localhost:3000/


### URLs
#### React webpage
http://localhost:3000/
#### Microservices Urls and Ports
Inventory Microservice: http://localhost:8100/<br>
Sales Microservice: http://localhost:8080/<br>
Services Microservice: http://localhost:8090/<br>


#### Technicians CRUDs
<b>GET technicians</b> http://localhost:8080/api/technicians/<br>
Returns a JSON object with the 'technicians' key, which is a list of all Technicians.

    {
        "technicians": [
            {
                "href": technician's reference url,
                "first_name": technician's first name,
                "last_name": technician's last_name,
                "employee_id": technician's employee_id,
                "id", technician's db id
            }
        ]
    }

<b>POST technicians</b> http://localhost:8080/api/technicians/<br>
Takes a JSON body including the following information:

    {
        "first_name": the technician's first name,
        "last_name": the technician's last name,
        "employee_id": the technician's employee_id
    }

On a successful request, returns the following information:

    {
        "href": technician's reference url,
        "first_name": technician's first name,
        "last_name": technician's last_name,
        "employee_id": technician's employee_id,
        "id", technician's db id
    }

<b>DELETE technician</b> http://localhost:8080/api/technicians/int:id/<br>
Removes the specified Technician from the database, and returns a JSON object with a deletion message:

    {
        "message": "deleted Technician /id/"
    }

#### Appointment CRUDs
<b>GET appointments</b> http://localhost:8080/api/appointments/<br>
Returns a JSON object with the 'appointments' key, which is a list of all appointments.

    {
        "appointments": [
            {
                "href": appointment's reference url,
                "date_time": iso format date_time,
                "vin": vin number of the serviced car,
                "customer": customer first and last name,
                "technician": {
                    "href": technician's refernce url,
                    "first_name": technician's first name,
                    "last_name": technician's last name,
                    "employee_id": technician's employee id,
                    "id": technician's db id
                },
                "reason": the reason for the appointment,
                "status": the appointment status: "created", "canceled", or "finished"
            },
        ]
    }

<b>POST appointments</b> http://localhost:8080/api/appointments/<br>
Takes a JSON body including the following information:

    {
        "date_time": iso format date_time,
        "reason": the reason for the appointment,
        "vin": vin number of the serviced car,
        "customer": the customer's first and last name,
        "technician": the technician's db id
    }

On a successful request, returns the following information:

    {
        "href": appointment's reference url,
        "date_time": iso format date_time,
        "vin": vin number of the serviced car,
        "customer": customer first and last name,
        "technician": {
            "href": technician's refernce url,
            "first_name": technician's first name,
            "last_name": technician's last name,
            "employee_id": technician's employee id,
            "id": technician's db id
        },
        "reason": the reason for the appointment,
        "status": the appointment status: "created", "canceled", or "finished"
    },

<b>DELETE appointment</b> http://localhost:8080/api/appointments/int:id/<br>
Removes the specified Appointment from the database, and returns a JSON object with a deletion message:

    {
        "message": "deleted Appointment /id/"
    }

<b>PUT appointment</b> http://localhost:8080/api/appointments/int:id/cancel/ & http://localhost:8080/api/appointments/int:id/finish/<br>
Changes the status of the specified Appointment to "canceled" or "finished" depending on the url invoked.

On a successful request, returns a JSON object containing "message" and "status" indicating the change:

    {
        "message": Appointment id: int:id,
        "status": status
    }

#### Customer CRUDs
<b>GET all customers</b> http://localhost:8090/api/customers/<br>
Returns JSON object with key, 'customers', which value is a list of all the customers.

    {
        "customers": [
            {
                "first_name": customer's first name,
                "last_name": customer's last name,
                "address": customer's address,
                "phone_number": customer's phone number,
                "id": customer id in database
            }
        ]
    }

<b>POST customer</b> http://localhost:8090/api/customers/<br>
Takes a JSON body including the following information:

    {
        "first_name": customer's first name,
        "last_name": customer's last name,
        "address": customer's address,
        "phone_number": customer's phone number,
    }

On a successful request, returns the following information:

    {
        "first_name": customer's first name,
        "last_name": customer's last name,
        "address": customer's address,
        "phone_number": customer's phone number,
        "id": customer id in database
    }

<b>DELETE customer</b> http://localhost:8090/api/customers/int:id/<br>
Removes the specified Customer from the database, and returns a JSON object with a deletion message:

    {
        "deleted": true
    }

#### Salespeople CRUDs
<b>GET all salespeople</b> http://localhost:8090/api/salespeople/<br>
Returns JSON object with key, 'salespeople', which hold a value that is a list of all the salespeople.

{
	"salespeople": [
		{
			"first_name": salesperson's first name,
			"last_name": salesperson's last name,
			"employee_id": salesperson's employee id,
			"id": salesperson id in the database
		}
	]
}

<b>POST salesperson</b> http://localhost:8090/api/salespeople/<br>
Takes a JSON body including the following information:

    {
        "first_name": salesperson's first name,
        "last_name": salesperson's last name,
        "employee_id": salesperson's employee id
    }

On a successful request, returns the following information:

    {
        "first_name": salesperson's first name,
        "last_name": salesperson's last name,
        "employee_id": salesperson's employee id,
        "id": salesperson id in the database
    }

<b>DELETE salesperson</b> http://localhost:8090/api/salespeople/int:id/<br>
Removes the specified salesperson from the database, and returns a JSON object with a deletion message:

    {
        "deleted": true
    }

#### Sales CRUDs
<b>GET all sales</b> http://localhost:8090/api/sales/<br>
Returns JSON object with key, 'sales', which hold a value that is a list of all the sales.

{
	"sales": [
		{
			"automobile": {
				"vin": the vin of the automobile that was sold. This is from the Value Object,
				"sold": the sales status of the automobile (should be true if it is in the list of sales)
			"salesperson": {
				"first_name": salesperson who sold the car's first name ,
				"last_name": salesperson who sold the car's last name ,
				"employee_id": salesperson who sold the car's employee id ,
				"id": salesperson who sold the car's id in the salesperson database
			},
			"customer": {
				"first_name": customer who bought the car's first name,
				"last_name": customer who bought the car's last name,
				"address": customer who bought the car's address,
				"phone_number": customer who bought the car's phone number,
				"id": customer who bought the car's id in the customer database
			},
			"price": the price of the automobile that was sold,
			"id": the id in the database of the automobile that was sold
		}
	]
}

<b>POST sale</b> http://localhost:8090/api/sales/<br>
Takes a JSON body including the following information:

{
	"automobile": a VIN belonging to a car in inventory,
	"salesperson": the id in the salesperson database assigned to the salesperson who sold the car,
	"customer": the id in the customer database assigned to the customer who bought the car,
	"price": the price of the car sold
}

On a successful request, returns the following information:

    {
        "automobile": {
            "vin": the vin of the automobile that was sold. This is from the Value Object,
            "sold": the sales status of the automobile (should be true if it is in the list of sales)
        "salesperson": {
            "first_name": salesperson who sold the car's first name ,
            "last_name": salesperson who sold the car's last name ,
            "employee_id": salesperson who sold the car's employee id ,
            "id": salesperson who sold the car's id in the salesperson database
        },
        "customer": {
            "first_name": customer who bought the car's first name,
            "last_name": customer who bought the car's last name,
            "address": customer who bought the car's address,
            "phone_number": customer who bought the car's phone number,
            "id": customer who bought the car's id in the customer database
        },
        "price": the price of the automobile that was sold,
        "id": the id in the database of the automobile that was sold
    }

<b>DELETE sale</b> http://localhost:8090/api/sales/int:id/<br>
Removes the specified sale from the database, and returns a JSON object with a deletion message:

    {
        "deleted": true
    }

#### Manufacturer CRUDs
<b>GET all manufacturers</b> http://localhost:8100/api/manufacturers/<br>
Returns JSON object with key, 'manufacturers', which hold a value that is a list of all the manufacturers.

{
	"manufacturers": [
		{
			"href": the reference to access that specific manufacturer,
			"id": the id belonging to the manufacturer in the manufacturers database
			"name": the manufacturer name
		}
	]
}

<b>POST manufacturer</b> http://localhost:8100/api/manufacturers/<br>
Takes a JSON body including the following information:

    {
        "name":"Chrysler"
    }

On a successful request, returns the following information:

    {
        "href": the reference to access that specific manufacturer,
        "id": the id belonging to the manufacturer in the manufacturers database
        "name": the manufacturer name
    }

<b>DELETE manufacturer</b> http://localhost:8100/api/manufacturers/int:id/<br>
Removes the specified manufacturer from the database, and returns a JSON object with a deletion message:

    {
        "id": the id belonging to the manufacturer in the manufacturers database (which is now null),
        "name": the manufacturer name
    }

#### Models CRUDs
<b>GET all models</b> http://localhost:8100/api/models/<br>
Returns JSON object with key, 'models', which hold a value that is a list of all the models.

    {
        "models": [
            {
                "href": the reference to access that specific model,
                "id": the id belonging to that specific model in the models database,
                "name": the model's name,
                "picture_url": a url with a picture of the model,
                "manufacturer": {
                    "href": the reference to access that specific manufacturer of the model,
                    "id": the id belonging to the manufacturer of the model in the manufacturers database
                    "name": the manufacturer of the model's name
                }
            }
        ]
    }

<b>POST models</b> http://localhost:8100/api/models/<br>
Takes a JSON body including the following information:

    {
        "name": the model's name,
        "picture_url": a url with a picture of the model,
        "manufacturer_id": the id in the manufacturers db that belongs to the manufacturer of the model
    }

On a successful request, returns the following information:

    {
        "href": the reference to access that specific model,
        "id": the id belonging to that specific model in the models database,
        "name": the model's name,
        "picture_url": a url with a picture of the model,
        "manufacturer": {
            "href": the reference to access that specific manufacturer of the model,
            "id": the id belonging to the manufacturer of the model in the manufacturers database
            "name": the manufacturer of the model's name
        }
    }

<b>DELETE models</b> http://localhost:8100/api/models/int:id/<br>
Removes the specified model from the database, and returns a JSON object with a deletion message:

    {
        "id": the id belonging to that specific model in the models database which should now be null,
        "name": the deleted model's name,
        "picture_url": a url with a picture of the deleted model,
        "manufacturer": {
            "href": the reference to access that specific manufacturer of the deleted model,
            "id": the id belonging to the manufacturer of the deleted model in the manufacturers database
            "name": the manufacturer of the deleted model's name
        }
    }

#### Automobiles CRUDs
<b>GET all automobiles</b> http://localhost:8100/api/automobiles/<br>
Returns JSON object with key, 'autos', which hold a value that is a list of all the automobiles.

    {
        "autos": [
            {
                "href": the path to access the specific automobile. Contains the vin,
                "id": the id in the automobiles database that is assigned to the automobile,
                "color": the automobile's color,
                "year": the automobile's year,
                "vin": the automobile's vin (should be 17 digits),
                "model": {
                    "href": the href of the automobile's model,
                    "id": the id in the models database of the the automobile's model,
                    "name": the name of the automobile's model,
                    "picture_url": the url of a picture of the automobile's model,
                    "manufacturer": {
                        "href": the href of the automobile's model's manufacturer,
                        "id": the id in the manufacturers database of the automobile's model's manufacturer,
                        "name": the name of the automobile's model's manufacturer
                    }
                },
                "sold": the status (sold?) of the automobile, true or false
            }
        ]
    }

<b>POST automobile</b> http://localhost:8100/api/automobiles/<br>
Takes a JSON body including the following information:

    {
        "color": the automobile's color,
        "year": the automobile's year,
        "vin": the automobile's vin (should be 17 digits),
        "model_id": the automobile's model's id in the models database
    }

On a successful request, returns the following information:

    {
        "href": the path to access the specific automobile. Contains the vin,
        "id": the id in the automobiles database that is assigned to the automobile,
        "color": the automobile's color,
        "year": the automobile's year,
        "vin": the automobile's vin (should be 17 digits),
        "model": {
            "href": the href of the automobile's model,
            "id": the id in the models database of the the automobile's model,
            "name": the name of the automobile's model,
            "picture_url": the url of a picture of the automobile's model,
            "manufacturer": {
                "href": the href of the automobile's model's manufacturer,
                "id": the id in the manufacturers database of the automobile's model's manufacturer,
                "name": the name of the automobile's model's manufacturer
            }
        },
        "sold": the status (sold?) of the automobile, true or false
    }

<b>DELETE automobile</b> http://localhost:8100/api/automobiles/int:vin/<br>
Removes the specified automobile from the database, and returns a JSON object with a deletion message:

    {
        "href": the path to access the deleted automobile. Contains the vin,
        "id": the id in the deleted automobiles database that is assigned to the automobile,
        "color": the deleted automobile's color,
        "year": the deleted automobile's year,
        "vin": the deleted automobile's vin (should be 17 digits),
        "model": {
            "href": the href of the deleted automobile's model,
            "id": the id in the models database of the the deleted automobile's model,
            "name": the name of the deleted automobile's model,
            "picture_url": the url of a picture of the deleted automobile's model,
            "manufacturer": {
                "href": the href of the deleted automobile's model's manufacturer,
                "id": the id in the manufacturers database of the deleted automobile's model's manufacturer,
                "name": the name of the deleted automobile's model's manufacturer
            }
        },
        "sold": the status (sold?) of the deleted automobile, true or false
    }
