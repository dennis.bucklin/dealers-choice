from django.urls import path

from .api_views import (
    api_technicians,
    api_technician,
    api_appointments,
    api_appointment,
    api_appt_cancel,
    api_appt_finish,
    api_auto_vo
)

urlpatterns = [
    path("technicians/", api_technicians, name="api_technicians",),
    path("technicians/<int:pk>/", api_technician, name="api_technician"),
    path("appointments/", api_appointments, name="api_appointments"),
    path("appointments/<int:pk>/", api_appointment, name="api_appointment"),
    path("appointments/<int:pk>/cancel/", api_appt_cancel, name="api_cancel"),
    path("appointments/<int:pk>/finish/", api_appt_finish, name="api_finish"),
    path("automobiles/", api_auto_vo, name="api_autos"),
]
