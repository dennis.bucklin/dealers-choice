from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField()


class CustomerVO(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    import_id = models.SmallIntegerField(unique=True)


class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.pk})


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.TextField()
    status = models.CharField(max_length=50)
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=50)

    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f"{self.date_time} - {self.status} - {self.vin}"

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"pk": self.pk})

    class Meta:
        ordering = ("date_time",)
