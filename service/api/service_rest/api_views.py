from .models import AutomobileVO, Appointment, Technician
from django.http import JsonResponse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from dateutil.parser import isoparse


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "sold",
        "vin",
    ]


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "vin",
        "customer",
        "technician",
        "reason",
        "status",
    ]
    encoders = {
        "technician": TechnicianListEncoder(),
    }


class TechnicianDetailEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "vin",
        "reason",
        "status",
        "customer",
        "technician",
    ]
    encoders = {
        "technician": TechnicianListEncoder(),
    }


@require_http_methods(["GET"])
def api_auto_vo(request):
    if request.method == "GET":
        autos = AutomobileVO.objects.all()
        return JsonResponse(
            {"autos": autos},
            encoder=AutomobileVOEncoder,
        )


@require_http_methods(["GET", "POST"])
def api_technicians(request):
    '''
    Multi-object API for Technicians

    GET:
    Returns a dictionary
    {
        "technician": [
            {
                "href": an api call for this object,
                "model": the model name of the shoe,
                "manufacturer": the manufacturer's name,
                "color": the color,
                "picture_url": the location of the picture,
                "bin": {
                    "import_href": an api call for the bin it's in
                }
            }
        ]
    }

    POST:
    On POST, creates a new Shoe. And returns it's details.
    {
        "model": "",
        "manufacturer": "",
        "color": "",
        "picture_url": "",
        "bin": ""
    }
    '''
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
        )
    else:
        content = json.loads(request.body)

        tech = Technician.objects.create(**content)
        return JsonResponse(
            tech,
            encoder=TechnicianListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianDetailEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse(
                {"message": "deleted Technician " + str(pk)}
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

    else:  # PUT
        content = json.loads(request.body)

        try:
            Technician.objects.filter(id=pk).update(**content)
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianDetailEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_appointments(request):
    '''
    Multi-object API for Technicians

    GET:
    Returns a dictionary
    {
        "technician": [
            {
                "href": an api call for this object,
                "model": the model name of the shoe,
                "manufacturer": the manufacturer's name,
                "color": the color,
                "picture_url": the location of the picture,
                "bin": {
                    "import_href": an api call for the bin it's in
                }
            }
        ]
    }

    POST:
    On POST, creates a new Shoe. And returns it's details.
    {
        "model": "",
        "manufacturer": "",
        "color": "",
        "picture_url": "",
        "bin": ""
    }
    '''
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        content["date_time"] = isoparse(content["date_time"])
        content["status"] = "created"
        try:
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Technician id"},
                status=400,
            )

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_appointment(request, pk):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentDetailEncoder,
                safe=False
            )
        except appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse(
                {"message": "deleted Appointment " + str(pk)}
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

    else:  # PUT
        content = json.loads(request.body)

        if "date_time" in content.keys():
            content["date_time"] = isoparse(content["date_time"])
        if "technician" in content.keys():
            try:
                technician = Technician.objects.get(id=content["technician"])
                content["technician"] = technician
            except Technician.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid Technician id"},
                    status=400,
                )
        try:
            Appointment.objects.filter(id=pk).update(**content)
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentDetailEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["PUT"])
def api_appt_cancel(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        setattr(appointment, "status", "canceled")
        appointment.save()
        return JsonResponse(
            {
                "message": "Appointment id: " + str(appointment.id),
                "status": appointment.status
            }
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "Does not exist"})
        response.status_code = 404
        return response


@require_http_methods(["PUT"])
def api_appt_finish(request, pk):
    try:
        appointment = Appointment.objects.get(id=pk)
        setattr(appointment, "status", "finished")
        appointment.save()
        return JsonResponse(
            {
                "message": "Appointment id: " + str(appointment.id),
                "status": appointment.status
            }
        )
    except Appointment.DoesNotExist:
        response = JsonResponse({"message": "Does not exist"})
        response.status_code = 404
        return response
