import React, { useEffect, useState } from 'react';

function TechnicianList() {
    const [technicians, setTechnicians] = useState([]);
    const [err, setErr] = useState('');  // error popup

    const getTechnicians = async () => {
        const techniciansUrl = "http://localhost:8080/api/technicians/";

        try {
            const response = await fetch(techniciansUrl);

            if(response.ok){
                const data = await response.json();
                setTechnicians(data.technicians);
            } else {
                setErr(
                    <div className={"alert alert-warning alert-dismissible"} role="alert">{"Invalid request at Technician API"}</div>
                );
            }
        } catch (error) {
            setErr(
                <div className={"alert alert-danger alert-dismissible"} role="alert">{"Failed to fetch at Technician API: " + error}</div>
            );
        }

    }

    useEffect(() => {
        getTechnicians();
    }, []);

    return (
        <>
        <h1>Technicians</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Employee ID</th>
            </tr>
          </thead>
          <tbody>
            {technicians.map(tech => {
              return(
                <tr key={tech.href}>
                  <td>{tech.first_name} {tech.last_name}</td>
                  <td>{tech.employee_id}</td>
                </tr>
              )
            })}
          </tbody>
        </table>
        <div className='liveAlertPlaceholder'>{err}</div>
        </>
    );
}
export default TechnicianList;
