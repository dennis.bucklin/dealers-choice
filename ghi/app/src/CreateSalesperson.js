import React, {useState} from 'react'

const initData = {
    employee_id:"",
    first_name:"",
    last_name:"",
}

function CreateSalesperson(){
    const [salespersonFormData, setSalespersonFormData] = useState(initData)

    const handleSubmit = async (event) => {
        event.preventDefault()

        const url = "http://localhost:8090/api/salespeople/"

        const fetchConfig = {
            method:"post",
            body:JSON.stringify(salespersonFormData),
            headers: {
                'Content-Type':'application/json',
            },
        }

        const salespersonResponse = await fetch(url, fetchConfig)

        if(salespersonResponse.ok){
            setSalespersonFormData(initData)
        }
    }

    const handleChange = (e) => {
        setSalespersonFormData({
            ...salespersonFormData,
            [e.target.name]:e.target.value
        })
    }

    return (
        <>
            <h1 className="mb-3">Create a Salesperson</h1>
            <form onSubmit={handleSubmit}>
                    <div className="form-floating mb-3">
                        <input value={salespersonFormData.employee_id} onChange={handleChange} placeholder="Employee ID" type="text" required name="employee_id" id="employee_id" className="form-control"/>
                        <label htmlFor="employee_id">Employee ID</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={salespersonFormData.first_name} onChange={handleChange} placeholder="First Name" type="text" required name="first_name" id="first_name" className="form-control"/>
                        <label htmlFor="first_name">First Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={salespersonFormData.last_name} onChange={handleChange} placeholder="Last Name" type="text" required name="last_name" id="last_name" className="form-control"/>
                        <label htmlFor="last_name">Last Name</label>
                    </div>
                    <button className="btn btn-warning">Create Salesperson</button>
                </form>
        </>
    )
}
export default CreateSalesperson
