import React, { useEffect, useState } from 'react'

function CustomerList() {
    const [customers, setCustomers] = useState([])

    const getCustomers = async () => {
        const customersUrl = "http://localhost:8090/api/customers/"

        try {
            const response = await fetch(customersUrl)
            if(response.ok){
                const data = await response.json()
                setCustomers(data.customers)
            } else {
                console.log("Invalid request at Customer API", response.status)
            }
        } catch (error) {
            console.error("Fetch failed", error)
        }

    }
    useEffect(() => {
        getCustomers()
    }, [])

    return (
        <>
            <h1>Customers</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Address</th>
                        <th>Phone Number</th>
                    </tr>
                </thead>
                <tbody>
                    {customers.map(customer => {
                        return(
                            <tr key={customer.id}>
                                <td>{customer.first_name}</td>
                                <td>{customer.last_name}</td>
                                <td>{customer.address}</td>
                                <td>{customer.phone_number}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}
export default CustomerList
