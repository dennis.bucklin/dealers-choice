function MainPage() {
    const time = new Date(Date.now()).getHours();

  return (
    <div className="">
      <h1 className="display-5 fw-bold text-warning" id="title">Dealer's Choice</h1>
      <div className="mx-auto">
        <p className="lead mb-4 text-warning" id="subtitle">
          {time < 12 ? "Good Morning! " :
           time < 18 ? "Good Afternoon! " :
           "Good Evening! "}
           Let's make a deal!
        </p>
      </div>
    </div>
  );
}

export default MainPage;
