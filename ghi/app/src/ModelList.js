import React, { useEffect, useState } from 'react'

function ModelList(){
    const [models, setModels] = useState([]);
    const [err, setErr] = useState('');

    const getModels = async () => {
        const modelUrl = "http://localhost:8100/api/models/";
        try {
            const response = await fetch(modelUrl);
            if(response.ok){
                const modelData = await response.json();
                setModels(modelData.models);
            } else {
                setErr(
                    <div className={"alert alert-warning alert-dismissible"} role="alert">{"Invalid request at Models API"}</div>
                )
            }
        } catch (e) {
            setErr(
                <div className={"alert alert-danger alert-dismissible"} role="alert">{"Failed to fetch at Appointments API: " + e}</div>
            )
        }
    }

    useEffect(() => {
        getModels()
    }, [])

    return (
        <>
          <h1>List of Models</h1>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Name</th>
                <th>Manufacturer</th>
                <th>Picture</th>
              </tr>
            </thead>
            <tbody>
              {models.map(model => {
                return(
                  <tr key={model.id}>
                    <td>{model.name}</td>
                    <td>{model.manufacturer.name}</td>
                    <td>
                    <img src={model.picture_url} alt="model"/>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
          <div className='liveAlertPlaceholder'>{err}</div>
        </>
        )
}
export default ModelList
