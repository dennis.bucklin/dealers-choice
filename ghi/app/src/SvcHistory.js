import React, { useEffect, useState } from 'react'

function SvcHistory() {
    const [appts, setAppts] = useState([]);
    const [err, setErr] = useState([]);  // error popup
    const [filterValue, setFilterValue] = useState("");

    const getAppts = async () => {
        const apptsUrl = "http://localhost:8080/api/appointments/";
        const autosUrl = "http://localhost:8080/api/automobiles/";

        try {
            const [apptResponse, autosResponse] = await Promise.all([
                fetch(apptsUrl),
                fetch(autosUrl),
            ]);

            if (apptResponse.ok && autosResponse.ok) {
                const [apptData, autosData] = await Promise.all([
                    apptResponse.json(),
                    autosResponse.json(),
                ]);

                // create VIP and format Date/Time
                for (const appt of apptData.appointments) {
                    appt.vip = false;
                    for (const auto of autosData.autos) {
                        if (auto.vin === appt.vin && auto.sold){
                            appt['vip'] = true;
                        }
                    }
                    const date_time = new Date(Date.parse(appt.date_time))
                    appt.date = date_time.toLocaleDateString();
                    appt.time = date_time.toLocaleTimeString();
                }

                // set Appt data
                setAppts(apptData.appointments);
            } else {
                setErr(
                    <div className={"alert alert-warning alert-dismissible"} role="alert">{"Request at Appointments API: " + apptResponse.status + "\nRequest at Automobiles API: " + autosResponse.status}</div>
                );
            }
        } catch (e) {
            setErr(
                <div className={"alert alert-danger alert-dismissible"} role="alert">{"Failed to fetch at Automobiles API: " + e}</div>
            );
        }
    }

    useEffect(() => {
        getAppts();
    }, []);

    const handleChange = (e) => {
        setFilterValue(e.target.value);
    }

    function getFilterValue() {
        return appts
            .filter((appt) =>
            appt['vin'].includes(filterValue)
            );
    }

    const handleReset = (e) => {
        setFilterValue('');
    }

    return (
        <>
        <h1>Service History</h1>
        <div className="input-group mb-3">
          <input
            className='form-control'
            onChange={handleChange}
            value={filterValue}
            type="text"
            name="filterValue"
            id="filterValue"
        />
          <button
            className="btn btn-warning"
            onClick={handleReset}
          >
            Reset
          </button>
        </div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Date</th>
              <th>Time</th>
              <th>VIP</th>
              <th>VIN</th>
              <th>Reason</th>
              <th>Customer Name</th>
              <th>Technician Name</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {getFilterValue().map(appt => {
              return(
                <tr key={appt.href}>
                  <td>{appt.date}</td>
                  <td>{appt.time}</td>
                  <td>{appt.vip ? "Yes": "No"}</td>
                  <td>{appt.vin}</td>
                  <td>{appt.reason}</td>
                  <td>{appt.customer}</td>
                  <td>{appt.technician.first_name} {appt.technician.last_name}</td>
                  <td>{appt.status}</td>
                </tr>
              )
            })}
          </tbody>
        </table>
        <div className='liveAlertPlaceholder'>{err}</div>
        </>
    );
}
export default SvcHistory;
