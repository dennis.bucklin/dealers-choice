import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid flex-wrap justify-content-center col">
      <ul className="nav justify-content-center">
        <li className='nav-item'><NavLink className="navbar-brand" to="/">Home</NavLink></li>
        <li className='nav-item'><NavLink className="navbar-brand" to="/autos/">Automobiles</NavLink></li>
        <li className='nav-item'><NavLink className="navbar-brand" to="/sales/">Sales</NavLink></li>
        <li className='nav-item'><NavLink className="navbar-brand" to="/appointments/">Appointments</NavLink></li>
        <li className='nav-item'><NavLink className="navbar-brand" to="/sales/create/">Create a Sale</NavLink></li>
        <li className='nav-item'><NavLink className="navbar-brand" to="/appointments/create/">Create Appointment</NavLink></li>
      </ul>
      <ul className="nav justify-content-center">
        <li className='nav-item dropdown m-2'>
          <button className="btn btn-warning dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">Sales</button>
          <ul className="dropdown-menu ps-2 bg-dark bg-dark">
            <li><NavLink className="navbar-brand text-warning" to="/customers/">Customers</NavLink></li>
            <li><NavLink className="text-warning navbar-brand" to="/salespeople/">Salespeople</NavLink></li>
            <li><NavLink className="text-warning navbar-brand" to="/salespeople/history/">Salesperson History</NavLink></li>
          </ul>
        </li>
        <li className='nav-item dropdown m-2'>
          <button className="btn btn-warning dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">Service</button>
          <ul className="dropdown-menu ps-2 bg-dark">
            <li><NavLink className="text-warning navbar-brand" to="/technicians/">Technicians</NavLink></li>
            <li><NavLink className="text-warning navbar-brand" to="/services">Service History</NavLink></li>
          </ul>
        </li>
        <li className='nav-item dropdown m-2'>
          <button className="btn btn-warning dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">Manage People</button>
          <ul className="dropdown-menu ps-2 bg-dark">
            <li><NavLink className="text-warning navbar-brand" to="/salespeople/create/">Create a Salesperson</NavLink></li>
            <li><NavLink className="text-warning navbar-brand" to="/customers/create/">Create a Customer</NavLink></li>
            <li><NavLink className="text-warning navbar-brand" to="/technicians/create/">Create a Technician</NavLink></li>
          </ul>
        </li>
        <li className='nav-item dropdown m-2'>
          <button className="btn btn-warning dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">Manage Inventory</button>
          <ul className="dropdown-menu ps-2 bg-dark">
            <li><NavLink className="text-warning navbar-brand" to="/mfg">Manufacturers</NavLink></li>
            <li><NavLink className="text-warning navbar-brand" to="/models/">Models</NavLink></li>
            <li><NavLink className="text-warning navbar-brand" to="/mfg/create/">Create Manufacturer</NavLink></li>
            <li><NavLink className="text-warning navbar-brand" to="/models/create/">Create Model</NavLink></li>
            <li><NavLink className="text-warning navbar-brand" to="/autos/create/">Create Automobile</NavLink></li>
          </ul>
        </li>
      </ul>
      </div>
    </nav>
  )
}

export default Nav;
