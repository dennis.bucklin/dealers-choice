import React, {useState, useEffect } from 'react'

const initData = {
    automobile:"",
    salesperson:"",
    customer:"",
    price:"",
}

function CreateSale() {
    const [autos, setAutos] = useState([])
    const [salespeople, setSalespeople] = useState([])
    const [customers, setCustomers] = useState([])

    const getAutos = async () => {
        const autosUrl = "http://localhost:8090/api/autovos/"
        const response = await fetch(autosUrl)
        if (response.ok){
            const data = await response.json()
            setAutos(data.autoVO)
        } else {
            console.error("Request Error")
        }
    }

    const getSalespeople = async () => {
        const salespeopleUrl = "http://localhost:8090/api/salespeople/"
        const response = await fetch(salespeopleUrl)

        if (response.ok){
            const data = await response.json()
            setSalespeople(data.salespeople)
        } else {
            console.error("Request Error")
        }
    }

    const getCustomers = async () => {
        const customersUrl = "http://localhost:8090/api/customers/"
        const response = await fetch(customersUrl)

        if (response.ok){
            const data = await response.json()
            setCustomers(data.customers)
        } else {
            console.error("Request Error")
        }
    }

    useEffect(()=>{
        getAutos()
    }, [])

    useEffect(()=>{
        getSalespeople()
    }, [])

    useEffect(()=>{
        getCustomers()
    }, [])

    const [formData, setFormData] = useState(initData)

    const handleSubmit = async (event) => {
        event.preventDefault()

        const url = "http://localhost:8090/api/sales/"

        const fetchConfig = {
            method:"post",
            body:JSON.stringify(formData),
            headers: {
                'Content-Type':'application/json',
            },
        }
        const saleResponse = await fetch(url, fetchConfig)

        if (saleResponse.ok){
            const updateAutoFetchConfig = {
                method: "PUT",
                body:JSON.stringify({"sold":true}),
                headers: {
                    'Content-Type':'application/json',
                },
            }
            const updateAutoUrl = `http://localhost:8100/api/automobiles/${formData.automobile}/`
            await fetch(updateAutoUrl, updateAutoFetchConfig)
            setFormData(initData)
        }
    }

    const handleChange = (e) => {
        setFormData({
          ...formData,
          [e.target.name]:e.target.value
        })
    }

    function getOnlyUnsold() {
        return autos
            .filter((auto) =>
                !auto.sold
            )
    }

    return(
        <>
            <h1 className="mb-3">Create a Sale</h1>
            <form onSubmit={handleSubmit}>
                <div className="mb-3">
                    <select onChange={handleChange} value={formData.automobile} required id="automobile" name="automobile" className="form-select">
                        <option value="">Choose an Automobile</option>
                        {getOnlyUnsold().map(auto => {
                            return (
                                <option value={auto.vin} key={auto.vin}>
                                    {auto.vin}
                                </option>
                            )
                        })}
                    </select>
                </div>
                <div className="mb-3">
                    <select onChange={handleChange} value={formData.salesperson} required id="salesperson" name="salesperson" className="form-select">
                        <option value="">Choose a Salesperson</option>
                        {salespeople.map(salesperson => {
                            return (
                                <option value={salesperson.id} key={salesperson.id}>
                                    {salesperson.first_name} {salesperson.last_name}
                                </option>
                            )
                        })}
                    </select>
                </div>
                <div className="mb-3">
                    <select onChange={handleChange} value={formData.customer} required id="customer" name="customer" className="form-select">
                        <option value="">Choose a Customer</option>
                        {customers.map(customer => {
                            return (
                                <option value={customer.id} key={customer.id}>
                                    {customer.first_name} {customer.last_name}
                                </option>
                            )
                        })}
                    </select>
                </div>
                <div className="form-floating mb-3">
                        <input value={formData.price} onChange={handleChange} placeholder="Price" type="text" required name="price" id="price" className="form-control"/>
                        <label htmlFor="price">Price</label>
                    </div>
                <button className="btn btn-warning">Create Sale</button>
            </form>
        </>
    )
}
export default CreateSale
