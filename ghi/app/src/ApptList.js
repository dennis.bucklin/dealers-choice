import React, { useEffect, useState } from 'react'

function ApptList() {
    const [appts, setAppts] = useState([]);
    const [err, setErr] = useState('');    // error popup

    const getAppts = async () => {
        const apptsUrl = "http://localhost:8080/api/appointments/"
        const autosUrl = "http://localhost:8080/api/automobiles/"

        try {
            // retrieve data from appointment and automobileVO APIs
            const [apptResponse, autosResponse] = await Promise.all([
                fetch(apptsUrl),
                fetch(autosUrl)
            ])

            if (apptResponse.ok && autosResponse.ok) {
                const [apptData, autosData] = await Promise.all([
                    apptResponse.json(),
                    autosResponse.json()
                ])

                // create a list of appointments that are status "created"
                const found = [];
                for (const appt of apptData.appointments){
                    if (appt.status === "created") {
                        appt.vip = false;
                        for (const auto of autosData.autos) {
                            if (auto.vin === appt.vin) {
                                appt['vip'] = true;
                            }
                        }
                        found.push(appt);
                    }
                }

                setAppts(found);
            } else {
                setErr(
                    <div className={"alert alert-warning alert-dismissible"} role="alert">{"Request at Appointments API: " + apptResponse.status + "\nRequest at Automobiles API: " + autosResponse.status}</div>
                )
            }
        } catch (e) {
            setErr(
                <div className={"alert alert-danger alert-dismissible"} role="alert">{"Failed to fetch at Automobiles API: " + e}</div>
            )
        }
    }

    useEffect(() => {
        getAppts()
    }, [])

    const handleStatus = async (e) => {
        const type = e.target.innerText.toLowerCase()
        const apptUrl = `http://localhost:8080${e.target.value}${type}/`
        const fetchConfig = {
            method: 'put',
        }
        try {
            const response = await fetch(apptUrl, fetchConfig)
            if(response.ok){
                getAppts()
            } else {
                setErr(
                    <div className={"alert alert-warning alert-dismissible"} role="alert">{"Invalid request at Appointments API"}</div>
                )
            }
        } catch (e) {
            setErr(
                <div className={"alert alert-danger alert-dismissible"} role="alert">{"Failed to fetch at Appointments API: " + e}</div>
            )
        }
    }

    return (
        <>
        <h1>Appointments</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Date</th>
              <th>Time</th>
              <th>VIP</th>
              <th>VIN</th>
              <th>Reason</th>
              <th>Customer Name</th>
              <th>Technician Name</th>
              <th>Cancel/Finish</th>
            </tr>
          </thead>
          <tbody>
            {appts.map(appt => {
              const date_time = new Date(Date.parse(appt.date_time))
              return(
                <tr key={appt.href}>
                  <td>{date_time.toLocaleDateString()}</td>
                  <td>{date_time.toLocaleTimeString()}</td>
                  <td>{appt.vip ? "Yes": "No"}</td>
                  <td>{appt.vin}</td>
                  <td>{appt.reason}</td>
                  <td>{appt.customer}</td>
                  <td>{appt.technician.first_name} {appt.technician.last_name}</td>
                  <td>
                        <button onClick={handleStatus} value={appt.href} className="btn btn-danger me-3">Cancel</button>
                        <button onClick={handleStatus} value={appt.href} className="btn btn-secondary">Finish</button>
                  </td>
                </tr>
              )
            })}
          </tbody>
        </table>
        <div className='liveAlertPlaceholder'>{err}</div>
        </>
    )
}

export default ApptList
