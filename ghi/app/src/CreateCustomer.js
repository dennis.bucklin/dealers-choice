import React, {useState} from 'react'

const initData = {
    first_name:"",
    last_name:"",
    address:"",
    phone_number:"",
}

function CreateCustomer(){
    const [customerFormData, setCustomerFormData] = useState(initData)

    const handleSubmit = async (event) => {
        event.preventDefault()

        const url = "http://localhost:8090/api/customers/"

        const fetchConfig = {
            method:"post",
            body:JSON.stringify(customerFormData),
            headers: {
                'Content-Type':'application/json',
            },
        }

        const customerResponse = await fetch(url, fetchConfig)

        if (customerResponse.ok){
            setCustomerFormData(initData)
        }
    }

    const handleChange = (e) => {
        setCustomerFormData({
            ...customerFormData,
            [e.target.name]:e.target.value
        })
    }

    return (
        <>
            <h1 className="mb-3">Create a Customer</h1>
            <form onSubmit={handleSubmit}>
                    <div className="form-floating mb-3">
                        <input value={customerFormData.first_name} onChange={handleChange} placeholder="First Name" type="text" required name="first_name" id="first_name" className="form-control"/>
                        <label htmlFor="first_name">First Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={customerFormData.last_name} onChange={handleChange} placeholder="Last Name" type="text" required name="last_name" id="last_name" className="form-control"/>
                        <label htmlFor="last_name">Last Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={customerFormData.address} onChange={handleChange} placeholder="address" type="text" required name="address" id="address" className="form-control"/>
                        <label htmlFor="address">Address</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={customerFormData.phone_number} onChange={handleChange} placeholder="Phone Number" type="text" required name="phone_number" id="phone_number" className="form-control"/>
                        <label htmlFor="phone_number">Phone Number</label>
                    </div>
                    <button className="btn btn-warning">Create Customer</button>
                </form>
        </>
    )
}
export default CreateCustomer
