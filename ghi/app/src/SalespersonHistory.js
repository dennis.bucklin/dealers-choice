import React, {useState, useEffect } from 'react'

function SalespersonHistory() {
    const [salespeople, setSalespeople] = useState([])
    const [salesperson, setSalesperson] = useState('')
    const [sales, setSales] = useState([])

    const getSalespeople = async () => {
        const salespeopleUrl = "http://localhost:8090/api/salespeople/"
        const response = await fetch(salespeopleUrl)

        if (response.ok){
            const data = await response.json()
            setSalespeople(data.salespeople)
        } else {
            console.error("Request Error - Salespeople")
        }
    }

    const getSales = async () => {
        const salesUrl = "http://localhost:8090/api/sales/"
        const response = await fetch(salesUrl)

        if (response.ok){
            const data = await response.json()
            setSales(data.sales)
        } else {
            console.error("Request Error - Sales")
        }
    }

    useEffect(()=>{
        getSalespeople()
    }, [])

    useEffect(()=>{
        getSales()
    }, [])

    const handleChangeSalesperson = (event) => {
        const value = event.target.value
        setSalesperson(value)
    }

    function filterSales() {
        return sales
            .filter((sale) =>
                sale.salesperson.id.toString() === salesperson
            )

    }

    return (
        <>
            <h1>Salesperson History</h1>
            <div className="mb-3">
                <select onChange={handleChangeSalesperson} value={salesperson} required id="salesperson" name="salesperson" className="form-select">
                    <option value="">Choose a Salesperson</option>
                    {salespeople.map(salesperson => {
                        return (
                            <option value={salesperson.id} key={salesperson.id}>
                                {salesperson.first_name} {salesperson.last_name}
                            </option>
                        )
                    })}
                </select>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {filterSales().map(sale => {
                        return(
                            <tr key={sale.id}>
                                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>${sale.price}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}
export default SalespersonHistory
