import React, {useState, useEffect } from 'react'

const initialData = {
    name: "",
    picture_url: "",
    manufacturer_id: "",
}

function CreateModel(){
    const [mfgs, setMfgs] = useState([])
    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/');

        if (response.ok) {
            const data = await response.json();
            setMfgs(data.manufacturers)
        } else {
            console.error("Request Error")
        }
    }

    useEffect(()=>{
        getData()
    }, [])

    const [formData, setFormData] = useState(initialData);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = "http://localhost:8100/api/models/";

        const fetchConfig = {
            method:"post",
            body:JSON.stringify(formData),
            headers: {
                'Content-Type':'application/json',
            },
        }

        const modelResponse = await fetch(url, fetchConfig);

        if (modelResponse.ok){
            setFormData(initialData);
        }
    }

    const handleChange = (e) => {
        setFormData({
          ...formData,
          [e.target.name]:e.target.value
        })
    }


    return (
        <>
          <div className="row">
            <h1 className="mb-3">Create a new Model</h1>
            <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input value={formData.name} onChange={handleChange} placeholder="Name" type="text" required name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input value={formData.picture_url} onChange={handleChange} placeholder="url" type="url" required name="picture_url" id="picture_url" className="form-control"/>
                <label htmlFor="picture_url">Picture URL</label>
              </div>
              <div className="mb-3">
                <select onChange={handleChange} value={formData.manufacturer_id} required id="manufacturer_id" name="manufacturer_id" className="form-select">
                <option value="">Choose a manufacturer</option>
                {mfgs.map(mfg => {
                  return (
                    <option value={mfg.id} key={mfg.href}>
                      { mfg.name }
                    </option>
                  );
                })}
                </select>
              </div>
              <button className="btn btn-warning">Create Model</button>
            </form>
          </div>
        </>


    )
}
export default CreateModel;
