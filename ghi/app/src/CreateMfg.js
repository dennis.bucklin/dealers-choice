import React, {useState} from 'react'

function CreateMfg() {
    const [mfg, setMfg] = useState('');
    const [err, setErr] = useState('');

    const handleSubmit = async (event) => {
        event.preventDefault();
        const mfgData = {};
        mfgData.name = mfg;

        const url = "http://localhost:8100/api/manufacturers/";

        const fetchConfig = {
            method:"post",
            body:JSON.stringify(mfgData),
            headers: {
                'Content-Type':'application/json',
            },
        };

        try {
            const mfgResponse = await fetch(url, fetchConfig)

            if (mfgResponse.ok){
                setMfg('');
            } else {
                setErr(
                    <div className={"alert alert-warning alert-dismissible"} role="alert">{"Invalid request at Manufacturers API"}</div>
                );
            }
        } catch (e) {
            setErr(
                <div className={"alert alert-danger alert-dismissible"} role="alert">{"Failed to fetch at Manufacturers API: " + e}</div>
            );
        }
    }

    const handleChangeMfg = (event) => {
        const value = event.target.value;
        setMfg(value);
    }


    return (
        <>
          <div className="row">
            <h1 className="mb-3">Create a new Manufacturer</h1>
            <div className='liveAlertPlaceholder'>{err}</div>
            <form onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input value={mfg} onChange={handleChangeMfg} placeholder="Manufacturer" type="text" required name="mfg" id="mfg" className="form-control"/>
                <label htmlFor="mfg">Manufacturer</label>
              </div>
                <button className="btn btn-warning">Create Manufacturer</button>
            </form>
          </div>
        </>


    );
}
export default CreateMfg
