import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import CreateMfg from './CreateMfg';
import MfgList from './MfgList';
import CreateModel from './CreateModel';
import ModelList from './ModelList';
import AutoList from './AutoList';
import CreateAuto from './CreateAuto';
import SalespersonList from './SalespersonList';
import SaleList from './SaleList';
import TechnicianList from './TechnicianList';
import CreateTech from './CreateTech';
import CreateAppt from './CreateAppt';
import CustomerList from './CustomerList';
import ApptList from './ApptList';
import SvcHistory from './SvcHistory';
import CreateSalesperson from './CreateSalesperson';
import CreateCustomer from './CreateCustomer';
import CreateSale from './CreateSale';
import SalespersonHistory from './SalespersonHistory';
import './index.css'

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container" id="bg">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="mfg">
            <Route index element={<MfgList />} />
            <Route path="create" element={<CreateMfg />} />
          </Route>
          <Route path="models">
            <Route index element={<ModelList />} />
            <Route path="create" element={<CreateModel />} />
          </Route>
          <Route path="autos">
            <Route index element={<AutoList />} />
            <Route path="create" element={<CreateAuto />} />
          </Route>
          <Route path="salespeople">
            <Route index element={<SalespersonList />} />
            <Route path="create" element={<CreateSalesperson />}/>
            <Route path="history" element={<SalespersonHistory />}/>
          </Route>
          <Route path="customers">
            <Route index element={<CustomerList />}/>
            <Route path="create" element={<CreateCustomer />}/>
          </Route>
          <Route path="technicians">
            <Route index element={<TechnicianList />} />
            <Route path="create" element={<CreateTech />} />
          </Route>
          <Route path="sales">
            <Route index element={<SaleList />} />
            <Route path="create" element={<CreateSale />}/>
          </Route>
          <Route path="services" element= {<SvcHistory />} />
          <Route path="appointments">
            <Route index element={<ApptList />} />
            <Route path="create" element={<CreateAppt />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
