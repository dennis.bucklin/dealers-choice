import React, { useEffect, useState } from 'react'

function AutoList() {
    const [autos, setAutos] = useState([]);
    const [err, setErr] = useState('');    // error popup

    const getAutos = async () => {
        const autosUrl = "http://localhost:8100/api/automobiles/";

        try {
            const response = await fetch(autosUrl);
            if(response.ok) {
                const data = await response.json();
                setAutos(data.autos);
            } else {
                setErr(
                    <div className={"alert alert-warning alert-dismissible"} role="alert">{"Invalid request at Automobiles API"}</div>
                )
            }
        } catch (e) {
            setErr(
                <div className={"alert alert-danger alert-dismissible"} role="alert">{"Failed to fetch at Appointments API: " + e}</div>
            )
        }
    };

    useEffect(() => {
        getAutos()
    }, []);

    return (
        <>
          <h1>List of Automobiles</h1>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>VIN</th>
                <th>Color</th>
                <th>Year</th>
                <th>Model</th>
                <th>Manufacturer</th>
                <th>Sold</th>
              </tr>
            </thead>
            <tbody>
              {autos.map(auto => {
                return(
                  <tr key={auto.id}>
                    <td>{auto.vin}</td>
                    <td>{auto.color}</td>
                    <td>{auto.year}</td>
                    <td>{auto.model.name}</td>
                    <td>{auto.model.manufacturer.name}</td>
                    <td>{auto.sold ? 'yes' : 'no' }</td>
                  </tr>
                )
              })}
            </tbody>
          </table>
          <div className='liveAlertPlaceholder'>{err}</div>
        </>
        )
}

export default AutoList;
