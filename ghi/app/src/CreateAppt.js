import React, {useState, useEffect } from 'react'

const initialData = {
    vin: "",
    customer: "",
    date_time: "",
    reason: "",
    technician: "",
}

function CreateAppt(){
    const [techs, setTechs] = useState([]);
    const [err, setErr] = useState('');  // error popup

    const getData = async () => {
        try {
            const response = await fetch('http://localhost:8080/api/technicians/');

            if (response.ok) {
                const data = await response.json();
                setTechs(data.technicians);
} else {
setErr(
    <div className={"alert alert-warning alert-dismissible"} role="alert">{"Invalid request at Technicians API"}</div>
);
            }
        } catch (e) {
            setErr(
                <div className={"alert alert-danger alert-dismissible"} role="alert">{"Failed to fetch at Technician API: " + e}</div>
            );
        }
    }

    useEffect(()=>{
      getData();
    }, [])

    const [formData, setFormData] = useState(initialData);

    const handleSubmit = async (event) => {
        event.preventDefault();
        formData.date_time = new Date(formData.date_time + ":00")

        const url = "http://localhost:8080/api/appointments/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type':'application/json',
            },
        }

        try {
            const response = await fetch(url, fetchConfig);
            if (response.ok){
                setFormData(initialData);
            } else {
                setErr(
                    <div className={"alert alert-warning alert-dismissible"} role="alert">{"Invalid requests at Appointments API"}</div>
                );
            }
        }catch(e){
            setErr(
                <div className={"alert alert-danger alert-dismissible"} role="alert">{"Failed to fetch Appointments API: " + e}</div>
            );
        }
    }

    const handleChange = (e) => {
        setFormData({
          ...formData,
          [e.target.name]:e.target.value
        });
    }


    return (
        <>
            <div className="row">
                <h1 className="mb-3">Create a new appointment</h1>
                <div className='liveAlertPlaceholder'>{err}</div>
                <form onSubmit={handleSubmit}>
                    <div className="form-floating mb-3">
                      <input value={formData.vin} onChange={handleChange} placeholder="VIN" type="text" required name="vin" id="vin" className="form-control"/>
                      <label htmlFor="vin">VIN</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input value={formData.customer} onChange={handleChange} placeholder="Customer Name" type="text" required name="customer" id="customer" className="form-control"/>
                      <label htmlFor="customer">Customer Name</label>
                    </div>
                    <div className="form-floating mb-3">
                      <input value={formData.date_time} onChange={handleChange} placeholder="Date & Time" type="datetime-local" required name="date_time" id="date_time" className="form-control"/>
                      <label htmlFor="date_time">Date & Time</label>
                    </div>
                    <div className="mb-3">
                      <label htmlFor="reason" className="form-label">reason</label>
                      <textarea onChange={handleChange} value={formData.reason} required id="reason" name="reason" rows="2" className="form-control"></textarea>
                    </div>
                    <div className="mb-3">
                      <select onChange={handleChange} value={formData.technician} required id="technician" name="technician" className="form-select">
                    <option value="">Choose a technician</option>
                      {techs.map(tech => {
                        return (
                          <option value={tech.id} key={tech.href}>
                            { tech.first_name }
                          </option>
                        );
                      })}
                  </select>
                </div>
                    <button className="btn btn-warning">Create Appointment</button>
                </form>
            </div>
        </>


    );
}
export default CreateAppt;
