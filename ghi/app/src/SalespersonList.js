import React, { useEffect, useState } from 'react'

function SalespersonList() {
    const [salespeople, setSalespeople] = useState([])

    const getSalespeople = async () => {
        const salespeopleUrl = "http://localhost:8090/api/salespeople/"

        try {
            const response = await fetch(salespeopleUrl)
            if(response.ok){
                const data = await response.json()
                setSalespeople(data.salespeople)
            } else {
                console.error("Invalid request at Salesperson API", response.status)
            }
        } catch (error) {
            console.error("Fetch failed", error)
        }

    }
    useEffect(() => {
        getSalespeople()
    }, [])

    return (
        <>
            <h1>Salespeople</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople.map(salesperson => {
                        return(
                            <tr key={salesperson.id}>
                                <td>{salesperson.employee_id}</td>
                                <td>{salesperson.first_name}</td>
                                <td>{salesperson.last_name}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}
export default SalespersonList
