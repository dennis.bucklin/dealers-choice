import { useEffect, useState } from "react";

function MfgList() {
    const [mfgs, setMfgs] = useState([]);
    const [err, setErr] = useState('');  // error popup

    const getData = async () => {
        try {
            const response = await fetch('http://localhost:8100/api/manufacturers/');

            if (response.ok) {
                const data = await response.json();
                setMfgs(data.manufacturers);
            } else {
                setErr(
                    <div className={"alert alert-warning alert-dismissible"} role="alert">{"Invalid request at Manufacturers API"}</div>
                );
            }
        } catch (e) {
            setErr(
                <div className={"alert alert-danger alert-dismissible"} role="alert">{"Failed to fetch at Manufacturers API: " + e}</div>
            );
        }
    }

    useEffect(()=>{
      getData();
    }, [])

    return (
      <div>
      <h1>Manufacturers</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {mfgs.map(mfg => {
            return (
              <tr key={mfg.id}>
                <td>{ mfg.name }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <div className='liveAlertPlaceholder'>{err}</div>
      </div>
    );
}

export default MfgList;
