import React, {useState } from 'react'

const initialData = {
    first_name: "",
    last_name: "",
    employee_id: "",
}

function CreateTech(){
    const [formData, setFormData] = useState(initialData);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = "http://localhost:8080/api/technicians/";

        const fetchConfig = {
            method:"post",
            body:JSON.stringify(formData),
            headers: {
                'Content-Type':'application/json',
            },
        }

        const modelResponse = await fetch(url, fetchConfig);

        if (modelResponse.ok){
            setFormData(initialData);
        }
    }

    const handleChange = (e) => {
        setFormData({
          ...formData,
          [e.target.name]:e.target.value
        })
    }

    return (
        <>
        <div className="row">
          <h1 className="mb-3">Create a new Technician</h1>
          <form onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input value={formData.first_name} onChange={handleChange} placeholder="First name" type="text" required name="first_name" id="first_name" className="form-control"/>
              <label htmlFor="first_name">First name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={formData.last_name} onChange={handleChange} placeholder="Last name" type="text" required name="last_name" id="last_name" className="form-control"/>
              <label htmlFor="last_name">Last name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={formData.employee_id} onChange={handleChange} placeholder="Employee_id" type="text" required name="employee_id" id="employee_id" className="form-control"/>
              <label htmlFor="employee_id">Employee ID</label>
            </div>
            <button className="btn btn-primary">Create Technician</button>
          </form>
          </div>
        </>


    )
}
export default CreateTech;
