import React, {useState, useEffect } from 'react'

const initData = {
    color:"",
    year:"",
    vin:"",
    model_id:"",
}

function CreateAuto(){
    const [models, setModels] = useState([])
    const getModels = async () => {
        const modelsUrl = "http://localhost:8100/api/models/"
        const response = await fetch(modelsUrl)

        if (response.ok){
            const data = await response.json()
            setModels(data.models)
        } else {
            console.error("Request Error")
        }
    }

    useEffect(()=>{
        getModels()
    }, [])

    const [autoFormData, setAutoFormData] = useState(initData)

    const handleSubmit = async (event) => {
        event.preventDefault()

        const url = "http://localhost:8100/api/automobiles/"

        const fetchConfig = {
            method:"post",
            body:JSON.stringify(autoFormData),
            headers: {
                'Content-Type':'application/json',
            },
        }

        const modelResponse = await fetch(url, fetchConfig)

        if (modelResponse.ok){
            setAutoFormData(initData);
        }
    }

    const handleChange = (e) => {
        setAutoFormData({
          ...autoFormData,
          [e.target.name]:e.target.value
        })
    }

    return (
        <>
            <div className="row">
                <h1 className="mb-3">Add an automobile to inventory</h1>
                <form onSubmit={handleSubmit}>
                    <div className="form-floating mb-3">
                        <input value={autoFormData.color} onChange={handleChange} placeholder="Color" type="text" required name="color" id="color" className="form-control"/>
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={autoFormData.year} onChange={handleChange} placeholder="Year" type="text" required name="year" id="year" className="form-control"/>
                        <label htmlFor="color">Year</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={autoFormData.vin} onChange={handleChange} placeholder="VIN" type="text" required name="vin" id="vin" className="form-control"/>
                        <label htmlFor="vin">VIN</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleChange} value={autoFormData.model_id} required id="model_id" name="model_id" className="form-select">
                        <option value="">Choose a model</option>
                        {models.map(model => {
                            return (
                                <option value={model.id} key={model.id}>
                                    {model.name}
                                </option>
                            )
                        })}
                        </select>
                    </div>
                    <button className="btn btn-warning">Create Automobile</button>
                </form>
            </div>
        </>
    )
}
export default CreateAuto
